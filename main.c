/* Данный пример представляет из себя проект для Visual Studio 2008,
   демонстрирующий работы с модулем L502 на примере синхронного
   ввода данных с АЦП и цифровых линий.

   Настройки частот, размера читаемых данных и т.д. задаются с помощью макросов в
   начале программы.
   Настройки логических каналов - с помощью таблиц f_channels/f_ch_modes/f_ch_ranges.

   Пример выполняет прием заданного количества блоков данных заданного размера, после
   чего завершает сбор данных. Пример также показывает как выполнять обработку данных
   и определять начало кадра, в случае если в L502_ProcessData() передается не целое
   число кадров.

   По завершению приема данных программа завершается! 
   Чтобы увидить результат ставьте точку останова в конце программы. 



   Данный пример содержит проект для Visual Studio 2008, а также может быть собран
   gcc в Linux или mingw в Windows через makefile или с помощью cmake (подробнее
   в коментариях в соответствующих файлах).

   Для того чтобы собрать проект в Visual Studio, измените путь к заголовочным файлам
   (Проект (Project) -> Свойства (Properties) -> Свойства конфигурации (Configuration Properties)
   -> С/С++ -> Общие (General) -> Дополнительные каталоги включения (Additional Include Directories))
   на тот, где у вас лежат заголовочный файл l502api.h и измените путь к библиотекам
   (Проект (Project) -> Свойства (Properties) -> Свойства конфигурации (Configuration Properties) ->
   Компановщик (Linker) -> Общие (General) -> Дополнительные катологи библиотек (Additional Library Directories)).

   Внимание!!: Если Вы собираете проект под Visual Studio и взяли проект с сайта (а не из SDK),
   то для корректного отображения русских букв в программе нужно изменить кодировку
   или указать сохранение с сигнатурой кодировки для UTF-8:
   выберите Файл (File) -> Доболнительные параметры сохранения (Advanced Save Options)...
   и в поле Кодировка (Encoding) выберите Юникод (UTF8, с сигнатурой)/Unicode (UTF-8 with signature)
   и сохраните изменения в файле.

   */

#include "l502api.h"
#if _WIN32
#include <locale.h>
#endif
#include <stdio.h>
#include <stdlib.h>

/* количество используемых логических каналов */
#define ADC_LCH_CNT  3

/* частота сбора АЦП в Гц*/
#define ADC_FREQ          2000000
/* частота кадров (на логический канал). При ADC_FREQ/ADC_LCH_CNT - межкадровой задержки нет */
#define ADC_FRAME_FREQ    (ADC_FREQ/ADC_LCH_CNT)
/* частота синхронного ввода в Гц*/
#define DIN_FREQ          2000000


/* сколько отсчетов считываем за блок */
#define READ_BLOCK_SIZE   4096*200
/* сколько блоков считаем */
#define READ_BLOCK_CNT    20
/* таймаут на прием блока (мс) */
#define READ_TIMEOUT     2000




/* номера используемых физических каналов */
static uint32_t f_channels[ADC_LCH_CNT] = {0,4,6};
/* режимы измерения для каналов */
static uint32_t f_ch_modes[ADC_LCH_CNT] = {L502_LCH_MODE_DIFF, L502_LCH_MODE_DIFF, L502_LCH_MODE_DIFF};
/* диапазоны измерения для каналов */
static uint32_t f_ch_ranges[ADC_LCH_CNT] = {L502_ADC_RANGE_10, L502_ADC_RANGE_10, L502_ADC_RANGE_10};


/* настройка параметров модуля */
int32_t f_setup_params(t_l502_hnd hnd)
{
    int32_t err = 0, i;

    /* устанавливаем параметры логической таблицы АЦП */
    err = L502_SetLChannelCount(hnd, ADC_LCH_CNT);
    for (i=0; (i < ADC_LCH_CNT) && !err; i++)
        err = L502_SetLChannel(hnd, i, f_channels[i], f_ch_modes[i], f_ch_ranges[i], 0);

    /* устанавливаем частоты ввода для АЦП и цифровых входов */
    if (!err)
    {
        double f_adc = ADC_FREQ, f_frame = ADC_FRAME_FREQ, f_din = DIN_FREQ;
        err = L502_SetAdcFreq(hnd, &f_adc, &f_frame);
        if (!err)
            err = L502_SetDinFreq(hnd, &f_din);
        if (!err)
        {
            /* выводим реально установленные значения - те что вернули функции */
            printf("Установленны частоты:\n    Частота сбора АЦП = %0.0f\n"
                "    Частота на лог. канал = %0.0f\n    Частота цифрового ввода = %0.0f\n",
                f_adc, f_frame, f_din);
        }
    }

    /* записываем настройки в модуль */
    if (!err)
        err = L502_Configure(hnd, 0);

    /* разрешаем синхронные потоки */
    if (!err)
    {
        err = L502_StreamsEnable(hnd, L502_STREAM_ADC | L502_STREAM_DIN);
    }

    return err;
}

int main(void)
{
    int32_t err = 0;
    uint32_t ver;
    uint32_t dev_cnt;
    t_l502_hnd hnd = NULL;

#if _WIN32
    /* устанавливаем локаль, чтобы можно было выводить по-русски в CP1251 без перевода в OEM */
    setlocale(LC_CTYPE, "");
#endif
    /* получаем версию библиотеки */
    ver = L502_GetDllVersion();
    printf("Верисия библиотеки l502api.dll: %d.%d.%d\n", (ver >> 24)&0xFF, (ver>>16)&0xFF, (ver>>8)&0xFF);



    /********** Получение списка устройств и выбор, с каким будем реботать ******************/

    /* Получаем количество модулей в системе */
    err = L502_GetSerialList(NULL, 0, 0, &dev_cnt);
    if (err<0)
    {
        /* Ошибка получения списка серийных номеров */
        fprintf(stderr, "Ошибка получения списка модулей: %s\n", L502_GetErrorString(err));
    }
    else if (dev_cnt==0)
    {
        /* Не найдено ни одного модуля */
        printf("Не найдено ни одного модуля\n");
    }
    else
    {
        /* Выделяем плоский массив под dev_cnt серийных номеров размером dev_cnt*L502_SERIAL_SIZE */
        char (*serial_list)[L502_SERIAL_SIZE] =(char(*)[L502_SERIAL_SIZE]) malloc(dev_cnt*L502_SERIAL_SIZE);
        if (serial_list==NULL)
        {
            /* Ошибка выделения памяти */
            fprintf(stderr, "Ошибка выделения памяти...\n");
            err = L502_ERR_MEMORY_ALLOC;
        }
        else
        {
            /* получаем список серийный номеров */
            err = L502_GetSerialList(serial_list, dev_cnt, 0, NULL);
            if (err>0)
            {
                uint32_t i, dev_ind;
                /* сохраняем, сколько нам вернули серийных номеров */
                dev_cnt = err;
                err = 0;

                /* выводим их серийные номера */
                printf("Найденно %d модулей L502\n", dev_cnt);
                for (i=0; i < dev_cnt; i++)
                {
                    printf("Модуль № %d: %s\n", i, serial_list[i]);
                }

                /* выбираем нужный по введенному номеру модуля по порядку с клавиатуры */
                printf("Введите номер модуля, с которым хотите работать (от 0 до %d)\n", dev_cnt-1);
                fflush(stdout);
                scanf("%d", &dev_ind);

                if (dev_ind >= dev_cnt)
                {
                    printf("Неверно указан номер модуля...\n");
                }
                else
                {
                    /* если ввели номер правильно - создаем описатель */
                    hnd = L502_Create();
                    if (hnd==NULL)
                    {
                        fprintf(stderr, "Ошибка создания описателя модуля!");
                    }
                    else
                    {
                        /* устанавливаем связь с модулем */
                        err = L502_Open(hnd, serial_list[dev_ind]);
                        if (err)
                        {
                            fprintf(stderr, "Ошибка установления связи с модулем: %s!", L502_GetErrorString(err));
                            L502_Free(hnd);
                            hnd = NULL;
                        }
                    }
                }
            }

            /* Освобождаем выделенный массив под серийные номера */
            free(serial_list);
        }
    }


    /********************************** Работа с модулем **************************/
    /* если успешно выбрали модуль и установили с ним связь - продолжаем работу */
    if (hnd!=NULL)
    {
        /* получаем информацию */
        t_l502_info info;
        err = L502_GetDevInfo(hnd, &info);
        if (err)
        {
            fprintf(stderr, "Ошибка получения серийного информации о модуле: %s!", L502_GetErrorString(err));
        }
        else
        {
            /* выводим полученную информацию */
            printf("Установлена связь со следующим модулем:\n");
            printf(" Серийный номер          : %s\n", info.serial);
            printf(" Наличие ЦАП             : %s\n", info.devflags & L502_DEVFLAGS_DAC_PRESENT ? "Да" : "Нет");
            printf(" Наличие BlackFin        : %s\n", info.devflags & L502_DEVFLAGS_BF_PRESENT ? "Да" : "Нет");
            printf(" Наличие гальваноразвязки: %s\n", info.devflags & L502_DEVFLAGS_GAL_PRESENT ? "Да" : "Нет");
            printf(" Версия ПЛИС             : %d.%d\n", (info.fpga_ver >> 8) & 0xFF, info.fpga_ver & 0xFF);
            printf(" Версия PLDA             : %d\n", info.plda_ver);
        }

        /* получаем версию установленного драйвера */
        if (!err)
        {
            uint32_t ver;
            err = L502_GetDriverVersion(hnd, &ver);
            if (err)
            {
                fprintf(stderr, "Ошибка получения версии драйвера: %s!", L502_GetErrorString(err));
            }
            else
            {
                printf(" Версия драйвера         : %d.%d.%d\n",
                       (ver >> 24)&0xFF, (ver>>16)&0xFF, (ver>>8)&0xFF);
            }
        }



        if (!err)
        {
            /* настраиваем параметры модуля */
            err = f_setup_params(hnd);
            if (err)
                fprintf(stderr, "Ошибка настройки модуля: %s!", L502_GetErrorString(err));
        }


        /* запуск синхронного ввода-вывода */
        if (!err)
        {
            err = L502_StreamsStart(hnd);
            if (err)
                fprintf(stderr, "Ошибка запуска сбора данных: %s!\n", L502_GetErrorString(err));
        }


        if (!err)
        {
            int block;
            int32_t stop_err;

            for (block = 0;!err && (block < READ_BLOCK_CNT); block++)
            {
                int32_t rcv_size;
                uint32_t adc_size, din_size;
                
                /* массив для приема необработанных данных */
                static uint32_t rcv_buf[READ_BLOCK_SIZE];
                static double   adc_data[READ_BLOCK_SIZE];
                static uint32_t din_data[READ_BLOCK_SIZE];

                /* принимаем данные (по таймауту) */
                rcv_size = L502_Recv(hnd, rcv_buf, READ_BLOCK_SIZE, READ_TIMEOUT);
                /* результат меньше нуля означает ошибку */
                if (rcv_size<0)
                {
                    err = rcv_size;
                    fprintf(stderr, "Ошибка приема данных: %s\n", L502_GetErrorString(err));
                }
                else if (rcv_size>0)
                {
                    uint32_t first_lch;
                    /* получаем номер логическаого канала, которому соответствует первый отсчет АЦП в массиве */
                    L502_GetNextExpectedLchNum(hnd, &first_lch);

                    adc_size = sizeof(adc_data)/sizeof(adc_data[0]);
                    din_size = sizeof(din_data)/sizeof(din_data[0]);

                    /* обрабатываем принятые данные, распределяя их на данные АЦП и цифроых входов */
                    err = L502_ProcessData(hnd, rcv_buf, rcv_size, L502_PROC_FLAGS_VOLT,
                                          adc_data, &adc_size, din_data, &din_size);
                    if (err)
                    {
                        fprintf(stderr, "Ошибка обработки данных: %s\n", L502_GetErrorString(err));
                    }
                    else
                    {
                        uint32_t lch;

                        printf("Блок %3d. Обработано данных АЦП =%d, цифровых входов =%d\n",
                               block, adc_size, din_size);    
                        /* если приняли цифровые данные - выводим первый отсчет */
                        if (din_size)
                            printf("    din_data = 0x%05X\n", din_data[0]);

                        /* выводим по одному отсчету на канал. если обработанный блок
                           начинается не с начала кадра, то в данном примере для
                           вывода берем конец неполного кадра и начало следующего */
                        for (lch=0; lch < ADC_LCH_CNT; lch++)
                        {
                            /* определяем позицию первого отсчета, соответствующего заданному логическому каналу:
                               либо с конца не полного кадра, либо из начала следующего */
                            uint32_t pos = lch >= first_lch ? lch - first_lch : ADC_LCH_CNT-first_lch + lch;
                            if (pos <= adc_size)
                            {
                                printf("    lch[%d]=%6.4f\n", lch, adc_data[pos]);
                            }
                            else
                            {
                                printf("    lch[%d]= ---- \n", lch);
                            }
                        }
                        printf("\n");
                        fflush(stdout);
                    }
                }
            }

            /* останавливаем поток сбора данных (независимо от того, была ли ошибка) */
            stop_err = L502_StreamsStop(hnd);
            if (stop_err)
            {
                fprintf(stderr, "Ошибка останова сбора данных: %s\n", L502_GetErrorString(err));
                if (!err)
                    err = stop_err;
            }
        }

        /* закрываем связь с модулем */
        L502_Close(hnd);
        /* освобождаем описатель */
        L502_Free(hnd);
    }
    return err;
}
    //int32_t get_list_res = 0;



   

