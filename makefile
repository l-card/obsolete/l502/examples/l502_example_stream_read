# makefile для сборки примера с помощью компиляторов mingw (под Windows)
# или GCC (под Linux). Необходимо определить 3 переменные:
#
# CC                      - имя команды для вызова компилятора
# L502API_LIBRARIES_DIR   - путь к файлу .a или .so библиотеки l502api (если не стандартный)
# L502API_INCLUDE_DIR     - путь к заголовочным файлам l502api (l502api.h, lpcie.h, pstdint.h)
#
# Ниже приведено несколько примеров в закоментированном виде

#--- Linux с заголовками и библиотекой в стандартных директориях: компилятор GCC
#CC = gcc

#--- Вариант запуска из MSYS со стандартным 32-битным mingw 
#CC = gcc
#L502API_LIBRARIES_DIR = "/c/Program Files/L-Card/lpcie/lib/mingw"
#L502API_INCLUDE_DIR = "/c/Program Files/L-Card/lpcie/include"


#---  64-битный вариант mingw w64, идущий вместе с cygwin  --------
#CC = x86_64-w64-mingw32-gcc
#L502API_LIBRARIES_DIR = "/cygdrive/c/Program Files (x86)/L-Card/lpcie/lib/mingw64"
#L502API_INCLUDE_DIR = "/cygdrive/c/Program Files (x86)/L-Card/lpcie/include"

#---  32-битный вариант mingw w64, идущий вместе с cygwin  --------
#CC = i686-w64-mingw32-gcc
#L502API_LIBRARIES_DIR = "/cygdrive/c/Program Files (x86)/L-Card/lpcie/lib/mingw"
#L502API_INCLUDE_DIR = "/cygdrive/c/Program Files (x86)/L-Card/lpcie/include"

#---  32-битный вариант mingw, идущий вместе с cygwin  --------
#CC = i686-pc-mingw32-gcc
#L502API_LIBRARIES_DIR = "/cygdrive/c/Program Files (x86)/L-Card/lpcie/lib/mingw"
#L502API_INCLUDE_DIR = "/cygdrive/c/Program Files (x86)/L-Card/lpcie/include"


FLAGS =

ifdef L502API_LIBRARIES_DIR
	FLAGS += -L $(L502API_LIBRARIES_DIR)
endif

ifdef L502API_INCLUDE_DIR
	FLAGS += -I $(L502API_INCLUDE_DIR)
endif



all:
	$(CC) main.c $(FLAGS) -ll502api -o l502_stream_read
